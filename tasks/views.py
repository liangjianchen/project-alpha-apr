from django.shortcuts import render, redirect
from tasks.forms import TasksForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TasksForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TasksForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    tasks_details = Task.objects.filter(assignee=request.user)
    context = {
        "tasks_details": tasks_details,
    }
    return render(request, "tasks/detail.html", context)
